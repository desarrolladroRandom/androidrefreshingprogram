/*
 * Copyright 2018, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.trackmysleepquality.sleeptracker

import android.app.Application
import android.text.Spanned
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.android.trackmysleepquality.database.SleepDatabaseDao
import com.example.android.trackmysleepquality.database.SleepNight
import com.example.android.trackmysleepquality.formatNights
import kotlinx.coroutines.*

/**
 * ViewModel for SleepTrackerFragment.
 */
class SleepTrackerViewModel(val database: SleepDatabaseDao, application: Application) : AndroidViewModel(application) {

    private val viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private var _tonight = MutableLiveData<SleepNight>()
    private var _nights = database.getAllNights()

    private val _navigateToSleepQuality = MutableLiveData<SleepNight>()
    val navigateToSleepQuality: LiveData<SleepNight>
        get() = _navigateToSleepQuality

    private val _showSnackBarInfo = MutableLiveData<Boolean>()
    val showSnackbarInfo: LiveData<Boolean>
        get() = _showSnackBarInfo


    val nightsString: LiveData<Spanned> = Transformations.map(_nights) { nights ->
        formatNights(nights, application.resources)
    }

    val startButtonVisible = Transformations.map(_tonight) {
        null == it
    }

    val stopButtonVisible = Transformations.map(_tonight) {
        null != it
    }

    val clearButtonVisible = Transformations.map(_nights) {
        it?.isNotEmpty()
    }

    init {
        initializeTonight()
    }

    fun navigationDone() {
        _navigateToSleepQuality.value = null
    }

    fun snackbarDone() {
        _showSnackBarInfo.value = false
    }

    private fun initializeTonight() {
        viewModelScope.launch {
            _tonight.value = getTonightFromDataBase()
        }
    }

    private suspend fun getTonightFromDataBase(): SleepNight? {
        return withContext(Dispatchers.IO) {
            var night = database.getTonight()
            if (night?.endTimeMilli != night?.startTimeMilli) night = null
            return@withContext night
        }
    }

    fun onStartTracking() {
        viewModelScope.launch {
            val newNight = SleepNight()
            insertNewNightToDatabase(newNight)
            _tonight.value = getTonightFromDataBase()
        }
    }

    private suspend fun insertNewNightToDatabase(newNight: SleepNight) {
        withContext(Dispatchers.IO) {
            database.insert(newNight)
        }
    }

    fun onStopTracking() {
        viewModelScope.launch {
            val oldNight = _tonight.value ?: return@launch
            oldNight.endTimeMilli = System.currentTimeMillis()
            updateNightToDatabase(oldNight)
            _navigateToSleepQuality.value = oldNight
        }
    }

    private suspend fun updateNightToDatabase(oldNight: SleepNight) {
        withContext(Dispatchers.IO) {
            database.update(oldNight)
        }
    }

    fun clear() {
        viewModelScope.launch {
            clearDatabase()
            _tonight.value = null
            _showSnackBarInfo.value = true
        }
    }

    private suspend fun clearDatabase() {
        withContext(Dispatchers.IO) {
            database.clear()
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}


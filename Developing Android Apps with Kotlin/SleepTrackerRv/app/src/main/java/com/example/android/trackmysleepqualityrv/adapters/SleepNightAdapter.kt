package com.example.android.trackmysleepqualityrv.adapters

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.trackmysleepqualityrv.R
import com.example.android.trackmysleepqualityrv.database.SleepNight
import com.example.android.trackmysleepqualityrv.databinding.ListItemViewBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.ClassCastException

private const val ITEM_VIEW_TYPE_HEADER = 0
private const val ITEM_VIEW_TYPE_ITEM = 1

class SleepNightAdapter(val onClickListener: SleepNightClickListener) : ListAdapter<DataItem, RecyclerView.ViewHolder>(SleepNightDiffCallback()) {

    private val adapterScope = CoroutineScope(Dispatchers.Default)

    fun addHeaderAndSubmitList(list: List<SleepNight>?) {
        adapterScope.launch {
            val items = when (list) {
                null -> listOf(DataItem.Header)
                else -> listOf(DataItem.Header) + list.map { DataItem.SleepNightItem(it) }
            }
            withContext(Dispatchers.Main) {
                submitList(items)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is SleepNightViewHolder -> {
                val nightItem = getItem(position) as DataItem.SleepNightItem
                holder.bind(nightItem.sleepNight, onClickListener)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_HEADER -> TextViewHolder.from(parent)
            ITEM_VIEW_TYPE_ITEM -> SleepNightViewHolder.from(parent)
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is DataItem.Header -> ITEM_VIEW_TYPE_HEADER
            is DataItem.SleepNightItem -> ITEM_VIEW_TYPE_ITEM
        }
    }

    class SleepNightViewHolder private constructor(private val listItemViewBinding: ListItemViewBinding) : RecyclerView.ViewHolder(listItemViewBinding.root) {
        private val res: Resources = itemView.context.resources

        fun bind(sleepNight: SleepNight, onClickListener: SleepNightClickListener) {
            /**           listItemViewBinding.txtSleepLength.text = convertDurationToFormatted(sleepNight.startTimeMilli, sleepNight.endTimeMilli, res)
            //            listItemViewBinding.txtSleepQuality.text = convertNumericQualityToString(sleepNight.sleepQuality, res)
            //            listItemViewBinding.imgSleepQuality.setImageResource(when (sleepNight.sleepQuality) {
            //                0 -> R.drawable.ic_sleep_0
            //                1 -> R.drawable.ic_sleep_1
            //                2 -> R.drawable.ic_sleep_2
            //                3 -> R.drawable.ic_sleep_3
            //                4 -> R.drawable.ic_sleep_4
            //                5 -> R.drawable.ic_sleep_5
            //                else -> R.drawable.ic_sleep_active
            //            }**/
            listItemViewBinding.sleepNight = sleepNight
            listItemViewBinding.executePendingBindings()
            listItemViewBinding.clickListener = onClickListener
        }

        companion object {
            fun from(parent: ViewGroup): SleepNightViewHolder {
                //WITHOUT DATABINDING
                //val layoutInflater = LayoutInflater.from(parent.context).inflate(R.layout.list_item_view, parent, false)

                // USING DATABINDING
                val layoutInflater = LayoutInflater.from(parent.context)
                val itemDataBinding = ListItemViewBinding.inflate(layoutInflater, parent, false)

                return SleepNightViewHolder(itemDataBinding)
            }
        }
    }

    class TextViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        companion object {
            fun from(parent: ViewGroup): TextViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.header, parent, false)
                return TextViewHolder(view)
            }
        }
    }
}

class SleepNightDiffCallback : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.equals(newItem)
    }
}

class SleepNightClickListener(val clickListener: (sleepNight: SleepNight) -> Unit) {
    fun onClick(sleepNight: SleepNight) = clickListener(sleepNight)
}

sealed class DataItem {
    data class SleepNightItem(val sleepNight: SleepNight) : DataItem() {
        override val id = sleepNight.nightId
    }

    object Header : DataItem() {
        override val id = Long.MIN_VALUE
    }

    abstract val id: Long
}
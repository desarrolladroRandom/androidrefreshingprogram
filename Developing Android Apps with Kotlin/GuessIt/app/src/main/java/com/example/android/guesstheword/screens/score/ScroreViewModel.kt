package com.example.android.guesstheword.screens.score

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ScroreViewModel(finalScrore: Int) : ViewModel() {
    private var _finalScore = MutableLiveData<Int>()
    val finalScore: LiveData<Int>
        get() = _finalScore


    private var _playAgain = MutableLiveData<Boolean>()
    val playAgain: LiveData<Boolean>
        get() = _playAgain

    init {
        _finalScore.value = finalScrore
    }

    fun onPlayAgain() {
        _playAgain.value = true
    }

    fun onFinishPlayAgain() {
        _playAgain.value = false
    }
}
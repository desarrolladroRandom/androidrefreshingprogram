package com.example.aboutme

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import kotlinx.android.synthetic.main.activity_color_my_view.*

class ColorMyViewActivity : AppCompatActivity(), OnClickListener {
    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.box_one -> view.setBackgroundColor(Color.DKGRAY)
            R.id.box_two -> view.setBackgroundColor(Color.CYAN)
            R.id.box_three -> view.setBackgroundColor(Color.RED)
            R.id.box_four -> view.setBackgroundColor(Color.BLUE)
            R.id.box_five -> view.setBackgroundColor(Color.GREEN)
            R.id.btn_red -> box_three.setBackgroundColor(Color.RED)
            R.id.btn_green -> box_four.setBackgroundColor(Color.GREEN)
            R.id.btn_yellow -> box_five.setBackgroundColor(Color.YELLOW)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_color_my_view)
    }
}

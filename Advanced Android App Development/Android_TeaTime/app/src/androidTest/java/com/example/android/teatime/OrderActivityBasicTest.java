package com.example.android.teatime;

import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class OrderActivityBasicTest {
    @Rule
    public ActivityTestRule<OrderActivity> activityRule = new ActivityTestRule(OrderActivity.class);

    @Test
    public void clickIncrementButton_changesQuantityAndCost() {
        onView((withId(R.id.increment_button))).perform(click());
        onView(withId(R.id.quantity_text_view)).check(matches(withText("1")));

        onView((withId(R.id.cost_text_view))).check(matches(withText("5,00 €")));
    }
}

enum class Directions { NORTH, SOUTH, EAST, WEST, WELL, START, END }

class Game {
    var path: MutableList<Directions> = mutableListOf(Directions.START)
    var north = { path.add(Directions.NORTH) }
    var south = { path.add(Directions.SOUTH) }
    var east = { path.add(Directions.EAST) }
    var west = { path.add(Directions.WEST) }
    var end = {
        path.add(Directions.END)
        println("Game Over")
        println("Path $path")
        path.clear()
        false
    }

    fun move(where: () -> Boolean) {
        where.invoke()
    }

    fun makeMove(move: String?) {
        when (move) {
            "n" -> move { north() }
            "s" -> move { south() }
            "e" -> move { east() }
            "w" -> move { west() }
            else -> move { end() }
        }
    }

}

fun main(args: Array<String>) {
    var game = Game()
//    game.apply {
//        north()
//        south()
//        east()
//        west()
//        end()
//    }
//    println(game.path)

//    val numbers = listOf<Int>(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)
//    println(numbers.diviByParm { it.rem(3) })
//    println(numbers.diviByParm { it.rem(2) })
    var valid = true
    while (valid) {
        print("Enter a direction: n/s/e/w:")
        var direction = readLine()
        if ("nsew".contains(direction.toString())) {
            game.makeMove(direction)
        } else {
            println("Direction not valid")
            valid = false
        }
    }
    println(game.path)
}

fun List<Int>.diviByParm(block: (Int) -> Int): List<Int> {
    var resultat = mutableListOf<Int>()
    for (item in this) {
        if (block(item) == 0) {
            resultat.add(item)
        }
    }
    return resultat
}
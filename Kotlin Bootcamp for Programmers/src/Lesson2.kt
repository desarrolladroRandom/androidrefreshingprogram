fun main(args: Array<String>) {
    practiceTimeBasicOperations()
    practiceTimeVariables()
}

fun practiceTimeBasicOperations() {
    var nFish = 2
    val leftFish = nFish + 71 + 233 - 13
    val aquariums = leftFish % 30
}

fun practiceTimeVariables() {
    var rainbowColor = "orange"
    val blackColor = "black"

    rainbowColor = "blue"
    //blackColor = "red"    // Error val cannot be reassigned
}

fun practiceTimeNullability() {
    var greenColor = null
    var blueColor: String? = null
}

fun practiceTimeNullabilityLists() {
    var llista = listOf(null, null)
    var llista2: List<Int?> = listOf(null, null)
}

fun practiceTimeNullChecks() {
    var nullTest: Int? = null
    println(nullTest?.inc() ?: 0)
}

fun practiceTimeStringTemplates() {
    val trout = "trout"
    var haddock = "haddock"
    var snapper = "snapper"
    print("I like to eat $trout and $snapper, but not a big fan of $haddock.")
}

fun practiceTimeStringTemplates2() {
    var firstName = "Baby shark"
    when (firstName.length) {
        0 -> print("Some error happened")
        in 3..12 -> print("Good fish name")
        else -> print("Ok fish name")
    }
}


fun practiceTimeLoops() {
    (0..100).forEach { if (it % 7 == 0) println(it) }
}
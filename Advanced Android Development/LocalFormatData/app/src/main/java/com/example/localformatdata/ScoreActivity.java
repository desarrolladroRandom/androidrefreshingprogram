package com.example.localformatdata;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity {

    private int mScore1 = 0;
    private int mScore2 = 0;
    private boolean mNightMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

    }

    public void minus1(View view) {
        mScore1--;
        TextView score_text = (TextView) findViewById(R.id.score_1);
        if (score_text != null) {
            score_text.setText(String.valueOf(mScore1));
        }
    }

    public void plus1(View view) {
        mScore1++;
        TextView score_text = (TextView) findViewById(R.id.score_1);
        if (score_text != null) {
            score_text.setText(String.valueOf(mScore1));
        }
    }

    public void minus2(View view) {
        mScore2--;
        TextView score_text = (TextView) findViewById(R.id.score_2);
        if (score_text != null) {
            score_text.setText(String.valueOf(mScore2));
        }
    }

    public void plus2(View view) {
        mScore2++;
        TextView score_text = (TextView) findViewById(R.id.score_2);
        if (score_text != null) {
            score_text.setText(String.valueOf(mScore2));
        }
    }
}

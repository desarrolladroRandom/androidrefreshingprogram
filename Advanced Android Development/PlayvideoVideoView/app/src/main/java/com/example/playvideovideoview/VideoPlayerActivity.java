package com.example.playvideovideoview;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoPlayerActivity extends AppCompatActivity {
    private static final String PLAYPAUSE_STATUS = "play_pause_status_det";
    private static final String PLAYBACK_TIME = "play_time_det";

    private boolean btnPlayPauseStatus = true;
    private int currentPosition = 0;

    private Button btnPlayPause, btnNext10Secs, btnPrev10Secs;
    private MediaController mediaController;
    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        videoView = findViewById(R.id.videoview);
        btnPlayPause = findViewById(R.id.btnPlayPause);
        btnNext10Secs = findViewById(R.id.next10Secs);
        btnPrev10Secs = findViewById(R.id.prev10Secs);

        mediaController = new MediaController(this);
        mediaController.setMediaPlayer(videoView);
        videoView.setMediaController(mediaController);

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Toast.makeText(VideoPlayerActivity.this, "Playback completed", Toast.LENGTH_SHORT).show();
                btnPrev10Secs.setEnabled(false);
                btnNext10Secs.setEnabled(false);
                btnPlayPause.setText("Play");
                videoView.seekTo(1);
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                btnPrev10Secs.setEnabled(true);
                btnNext10Secs.setEnabled(true);
                btnPlayPause.setEnabled(true);
                if (currentPosition > 0) {
                    videoView.seekTo(currentPosition);
                } else {
                    videoView.seekTo(1);
                }
            }
        });

        String videoName = getIntent().getStringExtra("video_name");
        initializePlayer(videoName);
    }


    public void nextPrev10Secs(View view) {
        int currentPosition = videoView.getCurrentPosition();
        if (view.getId() == R.id.next10Secs) {
            videoView.seekTo(currentPosition + 10000);
        } else {
            if (currentPosition > 11)
                videoView.seekTo(currentPosition - 10000);
            else
                videoView.seekTo(0);
        }
    }

    public void playPauseVideo(View view) {
        if (!btnPlayPauseStatus) {
            btnPrev10Secs.setEnabled(false);
            btnNext10Secs.setEnabled(false);
            btnPlayPause.setText("Play");
            videoView.pause();
        } else {
            btnPrev10Secs.setEnabled(true);
            btnNext10Secs.setEnabled(true);
            btnPlayPause.setText("Pause");
            videoView.start();
        }
        btnPlayPauseStatus = !btnPlayPauseStatus;
    }

    private void initializePlayer(String videoName) {
        Uri videoUri = getMedia(videoName);
        videoView.setVideoURI(videoUri);

        if (currentPosition > 0) {
            videoView.seekTo(currentPosition);
        } else {
            videoView.seekTo(1);
        }
    }

    private Uri getMedia(String mediaName) {
        if (URLUtil.isValidUrl(mediaName)) {
            return Uri.parse(mediaName);
        } else {
            return Uri.parse("android.resource://" + getPackageName() + "/raw/" + mediaName);
        }
    }

}

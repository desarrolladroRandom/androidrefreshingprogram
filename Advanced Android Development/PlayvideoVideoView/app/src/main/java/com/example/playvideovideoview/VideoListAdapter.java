package com.example.playvideovideoview;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.VideoViewHolder> {
    private ArrayList<String> videoList;
    private LayoutInflater mInflater;
    private Context context;

    class VideoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final VideoListAdapter videoListAdapter;
        private final TextView videoTextView;

        private VideoViewHolder(View itemView, VideoListAdapter adapter) {
            super(itemView);
            videoTextView = itemView.findViewById(R.id.txt_video_name);
            this.videoListAdapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Log.e("XXXX", "onClick: ");
            String videoName = videoList.get(getAdapterPosition());
            Intent intent = new Intent(context, VideoPlayerActivity.class);
            intent.putExtra("video_name", videoName);
            context.startActivity(intent);
        }
    }


    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(R.layout.item_layout, parent, false);
        return new VideoViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(VideoViewHolder holder, int position) {
        String mCurrent = videoList.get(position);
        holder.videoTextView.setText(mCurrent);
    }

    public VideoListAdapter(Context context, ArrayList<String> videoList) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.videoList = videoList;
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

}
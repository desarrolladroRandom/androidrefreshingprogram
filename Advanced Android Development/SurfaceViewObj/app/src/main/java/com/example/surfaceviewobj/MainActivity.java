package com.example.surfaceviewobj;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private GameView mGameView;
    private MemoGame memoGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //mGameView = new GameView(this);
        //mGameView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

        memoGame = new MemoGame(this);
        setContentView(memoGame);
    }

    @Override
    protected void onPause() {
        super.onPause();
        memoGame.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        memoGame.resume();
    }
}

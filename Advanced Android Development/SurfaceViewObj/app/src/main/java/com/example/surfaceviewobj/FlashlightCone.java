package com.example.surfaceviewobj;

public class FlashlightCone {
    private int mRadius;
    private int mX;
    private int mY;

    public int getmRadius() {
        return mRadius;
    }

    public int getmX() {
        return mX;
    }

    public int getmY() {
        return mY;
    }

    public FlashlightCone(int viewWidth, int viewHeight) {
        mY = viewHeight / 2;
        mX = viewWidth / 2;
        mRadius = ((viewWidth <= viewHeight) ? mX / 3 : mY / 3);
    }

    public void update(int newX, int newY) {
        mX = newX;
        mY = newY;
    }
}

package com.example.sensorbasedorientation;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener, UpdateActivityInterface {
    // TextViews to display current sensor values.
    private TextView mTextSensorAzimuth, mTextSensorPitch, mTextSensorRoll;
    private ImageView spotTop, spotBottom, spotLeft, spotRight;

    // System sensor manager instance.
    private SensorManager mSensorManager;

    // Accelerometer and magnetometer sensors, as retrieved from the sensor manager.
    private Sensor mSensorAccelerometer, mSensorMagnetometer;

    // Very small values for the accelerometer (on all three axes) should
    // be interpreted as 0. This value is the amount of acceptable non-zero drift.
    private static final float VALUE_DRIFT = 0.05f;

    private float[] accelerometerData = new float[3];
    private float[] magnetometerData = new float[3];

    private Display display;


    private UpdateActivityInterface updateActivityInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        updateActivityInterface = this;

        // Lock the orientation to portrait
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mTextSensorAzimuth = findViewById(R.id.value_azimuth);
        mTextSensorPitch = findViewById(R.id.value_pitch);
        mTextSensorRoll = findViewById(R.id.value_roll);
        spotTop = findViewById(R.id.spot_top);
        spotRight = findViewById(R.id.spot_right);
        spotBottom = findViewById(R.id.spot_bottom);
        spotLeft = findViewById(R.id.spot_left);


        // Get accelerometer and magnetometer sensors from the sensor manager.
        // The getDefaultSensor() method returns null if the sensor
        // is not available on the device.
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    /**
     * Listeners for the sensors are registered in this callback so that
     * they can be unregistered in onStop().
     */
    @Override
    protected void onStart() {
        super.onStart();

        // Listeners for the sensors are registered in this callback and can be unregistered in onStop()
        // Check to ensure sensors are available before registering listeners.
        // Both listeners are registered with a "normal" amount of delay (SENSOR_DELAY_NORMAL).
        if (mSensorAccelerometer != null) {
            mSensorManager.registerListener(this, mSensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (mSensorMagnetometer != null) {
            mSensorManager.registerListener(this, mSensorMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Unregister all sensor listeners in this callback so they don't continue to use resources when the app is stopped.
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(final SensorEvent sensorEvent) {
        new MyAsyncTask(sensorEvent, updateActivityInterface).execute();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    @Override
    public void updateOnSensorChanged(DeviceInfo deviceInfo) {
        mTextSensorAzimuth.setText(getResources().getString(R.string.value_format, deviceInfo.azimuth));
        mTextSensorPitch.setText(getResources().getString(R.string.value_format, deviceInfo.pitch));
        mTextSensorRoll.setText(getResources().getString(R.string.value_format, deviceInfo.roll));

        spotTop.setAlpha(0f);
        spotBottom.setAlpha(0f);
        spotLeft.setAlpha(0f);
        spotRight.setAlpha(0f);

        if (deviceInfo.pitch > 0) {
            spotBottom.setAlpha(deviceInfo.pitch);
        } else {
            spotTop.setAlpha(Math.abs(deviceInfo.pitch));
        }
        if (deviceInfo.roll > 0) {
            spotLeft.setAlpha(deviceInfo.roll);
        } else {
            spotRight.setAlpha(Math.abs(deviceInfo.roll));
        }
    }

    class MyAsyncTask extends AsyncTask<Void, Integer, DeviceInfo> {
        private UpdateActivityInterface updateActivityInterface;
        private SensorEvent sensorEvent;

        MyAsyncTask(SensorEvent sensorEvent, UpdateActivityInterface updateActivityInterface) {
            this.updateActivityInterface = updateActivityInterface;
            this.sensorEvent = sensorEvent;
        }

        @Override
        protected void onPostExecute(DeviceInfo deviceInfo) {
            updateActivityInterface.updateOnSensorChanged(deviceInfo);
        }

        @Override
        protected DeviceInfo doInBackground(Void... voids) {
            int sensorType = sensorEvent.sensor.getType();
            float[] rotationMatrixAdjusted = new float[9];
            float[] orientationValues = new float[3];
            float[] rotationMatrix = new float[9];

            switch (sensorType) {
                case Sensor.TYPE_ACCELEROMETER:
                    accelerometerData = sensorEvent.values.clone();
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    magnetometerData = sensorEvent.values.clone();
                    break;
            }
            boolean rotationOK = SensorManager.getRotationMatrix(rotationMatrix, null, accelerometerData, magnetometerData);

            switch (display.getRotation()) {
                case Surface.ROTATION_0:
                    rotationMatrixAdjusted = rotationMatrix.clone();
                    break;
                case Surface.ROTATION_90:
                    SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, rotationMatrixAdjusted);
                    break;
                case Surface.ROTATION_180:
                    SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_MINUS_X, SensorManager.AXIS_MINUS_Y, rotationMatrixAdjusted);
                    break;
                case Surface.ROTATION_270:
                    SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_MINUS_Y, SensorManager.AXIS_X, rotationMatrixAdjusted);
                    break;
            }

            if (rotationOK) {
                SensorManager.getOrientation(rotationMatrixAdjusted, orientationValues);
            }

            float azimuth = orientationValues[0];
            float pitch = orientationValues[1];
            float roll = orientationValues[2];

            if (Math.abs(pitch) < VALUE_DRIFT) pitch = 0;
            if (Math.abs(roll) < VALUE_DRIFT) roll = 0;

            return new DeviceInfo(azimuth, pitch, roll);
        }
    }

    public void goToSecondActivity(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }
}
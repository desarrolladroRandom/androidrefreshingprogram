package com.example.sensorbasedorientation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements SensorEventListener, UpdateActivityInterface {
    private Sensor mSensorAccelerometer, mSensorMagnetometer;
    private SensorManager mSensorManager;

    private static final float VALUE_DRIFT = 0.05f;

    private float[] accelerometerData = new float[3];
    private float[] magnetometerData = new float[3];

    private Display display;

    private UpdateActivityInterface updateActivityInterface;

    private ImageView imgBubbleSpirit;
    private TextView txtBubbleSpirit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();

        imgBubbleSpirit = findViewById(R.id.bubble_spirit);
        txtBubbleSpirit = findViewById(R.id.bubble_spirit_label);

        updateActivityInterface = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mSensorAccelerometer != null) {
            mSensorManager.registerListener(this, mSensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (mSensorMagnetometer != null) {
            mSensorManager.registerListener(this, mSensorMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        new SecondActivity.MyAsyncTask(sensorEvent, updateActivityInterface).execute();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    @Override
    public void updateOnSensorChanged(DeviceInfo deviceInfo) {
        float screenHeight = getResources().getDisplayMetrics().xdpi;
        float screenWidth = getResources().getDisplayMetrics().ydpi;

        // Pitch -> top-to-bottom
        // Roll  -> left-to-right

//        int pitchInt = Math.round((deviceInfo.pitch * screenHeight) + (screenHeight / 2));
//        int rollInt = Math.round((deviceInfo.roll * screenWidth) + (screenWidth / 2));
        int pitchInt = Math.round((deviceInfo.pitch * screenHeight)+810);
        int rollInt = Math.round((deviceInfo.roll * screenWidth)+540);
        txtBubbleSpirit.setText(String.format("ROLL : %s PITCH : %s", rollInt, pitchInt));
        imgBubbleSpirit.setX(rollInt);
        imgBubbleSpirit.setY(pitchInt);
    }

    class MyAsyncTask extends AsyncTask<Void, Integer, DeviceInfo> {
        private UpdateActivityInterface updateActivityInterface;
        private SensorEvent sensorEvent;

        MyAsyncTask(SensorEvent sensorEvent, UpdateActivityInterface updateActivityInterface) {
            this.updateActivityInterface = updateActivityInterface;
            this.sensorEvent = sensorEvent;
        }

        @Override
        protected void onPostExecute(DeviceInfo deviceInfo) {
            updateActivityInterface.updateOnSensorChanged(deviceInfo);
        }

        @Override
        protected DeviceInfo doInBackground(Void... voids) {
            int sensorType = sensorEvent.sensor.getType();
            float[] rotationMatrixAdjusted = new float[9];
            float[] orientationValues = new float[3];
            float[] rotationMatrix = new float[9];

            switch (sensorType) {
                case Sensor.TYPE_ACCELEROMETER:
                    accelerometerData = sensorEvent.values.clone();
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    magnetometerData = sensorEvent.values.clone();
                    break;
            }
            boolean rotationOK = SensorManager.getRotationMatrix(rotationMatrix, null, accelerometerData, magnetometerData);

            switch (display.getRotation()) {
                case Surface.ROTATION_0:
                    rotationMatrixAdjusted = rotationMatrix.clone();
                    break;
                case Surface.ROTATION_90:
                    SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, rotationMatrixAdjusted);
                    break;
                case Surface.ROTATION_180:
                    SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_MINUS_X, SensorManager.AXIS_MINUS_Y, rotationMatrixAdjusted);
                    break;
                case Surface.ROTATION_270:
                    SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_MINUS_Y, SensorManager.AXIS_X, rotationMatrixAdjusted);
                    break;
            }

            if (rotationOK) {
                SensorManager.getOrientation(rotationMatrixAdjusted, orientationValues);
            }

            float azimuth = orientationValues[0];
            float pitch = orientationValues[1];
            float roll = orientationValues[2];

            if (Math.abs(pitch) < VALUE_DRIFT) pitch = 0;
            if (Math.abs(roll) < VALUE_DRIFT) roll = 0;

            return new DeviceInfo(azimuth, pitch, roll);
        }
    }
}

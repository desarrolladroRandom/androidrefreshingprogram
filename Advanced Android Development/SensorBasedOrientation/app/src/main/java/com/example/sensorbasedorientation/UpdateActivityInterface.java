package com.example.sensorbasedorientation;

public interface UpdateActivityInterface {
    void updateOnSensorChanged(DeviceInfo deviceInfo);
}

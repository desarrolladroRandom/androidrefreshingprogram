package com.example.memoryprofilertool;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.memoryprofilertool.R;

public class LargeImages extends AppCompatActivity {
    private ImageView imgDinosaur;
    private int toggle = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_large_images);
        imgDinosaur = findViewById(R.id.imgDinosaur);
    }

    public void swapImage(View view) {
        if (toggle == 0) {
            imgDinosaur.setBackgroundResource(R.drawable.dinosaur_medium);
            toggle = 1;
        } else {
            try {
                Thread.sleep(32); // two refreshes
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            imgDinosaur.setBackgroundResource(R.drawable.ankylo);
            toggle = 0;
        }
    }
}

package com.example.accesibleapps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
    int imgStatus = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ImageButton buttonImage = findViewById(R.id.button_image);
        buttonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imgStatus == 0) {
                    buttonImage.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_lock_black));
                    buttonImage.setContentDescription(getResources().getString(R.string.lock_button));
                    imgStatus = 1;
                } else {
                    buttonImage.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_delete_black));
                    buttonImage.setContentDescription(getResources().getString(R.string.delete_button));
                    imgStatus = 0;
                }
            }
        });

    }
}

package com.example.propertyanimations;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.graphics.Rect;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import static android.view.FrameMetrics.ANIMATION_DURATION;

public class MainActivity extends AppCompatActivity {
    private TextView myAnimatedTextView;
    private AnimationSet animationSet;

    private RotateAnimation rotate;
    private Animation increase;
    private Explode explode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myAnimatedTextView = findViewById(R.id.myAnimatedTextView);
        animationSet = new AnimationSet(false);

        rotate = new RotateAnimation(
                0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
        );
        rotate.setDuration(2000);

        increase = new ScaleAnimation(
                0f, 1f, // Start and end values for the X axis scaling
                0f, 1f,// Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
        increase.setDuration(2000);

        animationSet.addAnimation(rotate);
        animationSet.addAnimation(increase);

        myAnimatedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myAnimatedTextView.startAnimation(animationSet);
            }
        });
    }

}

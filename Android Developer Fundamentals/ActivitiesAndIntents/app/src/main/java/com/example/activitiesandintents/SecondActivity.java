package com.example.activitiesandintents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    public static final String EXTRA_REPLY = "com.example.activitiesandintents.extra.REPLY";
    private EditText editTextReply;
    private TextView textMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        textMessage = (TextView) findViewById(R.id.text_message);
        editTextReply = (EditText) findViewById(R.id.editText_reply);

        String message = getIntent().getStringExtra(MainActivity.EXTRA_MESSAGE);
        textMessage.setText(message);
    }

    public void returnReply(View view) {
        String reply = editTextReply.getText().toString();

        Intent replyIntent = new Intent();
        replyIntent.putExtra(EXTRA_REPLY,reply);
        setResult(RESULT_OK,replyIntent);
        finish();
    }
}

package com.example.activitiesandintents;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static final int TEXT_REQUEST = 1;
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    public static final String EXTRA_MESSAGE = "com.example.activitiesandintents.extra.MESSAGE";
    public static final String EXTRA_OPTION = "com.example.activitiesandintents.extra.OPTION";

    private EditText msgEditText;
    private TextView mReplyTextHeader, mRplyTextMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mReplyTextHeader = (TextView) findViewById(R.id.text_header_reply);
        mRplyTextMsg = (TextView) findViewById(R.id.text_reply_msg);
        msgEditText = (EditText) findViewById(R.id.edtxt_msg_main);
    }

    public void launchSecondActivity(View view) {
        String message = msgEditText.getText().toString();

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivityForResult(intent, TEXT_REQUEST);
    }

    public void launchThirdActivity(View view) {
        Intent intent = new Intent(this, ThirdActivity.class);
        switch (view.getId()) {
            case R.id.btn_msg1:
                intent.putExtra(EXTRA_OPTION, 1);
                break;
            case R.id.btn_msg2:
                intent.putExtra(EXTRA_OPTION, 2);
                break;
            case R.id.btn_msg3:
                intent.putExtra(EXTRA_OPTION, 3);
                break;
        }
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TEXT_REQUEST) {
            if (resultCode == RESULT_OK) {
                String reply = data.getStringExtra(SecondActivity.EXTRA_REPLY);
                mReplyTextHeader.setVisibility(View.VISIBLE);
                mRplyTextMsg.setVisibility(View.VISIBLE);
                mRplyTextMsg.setText(reply);
            }
        }
    }
}

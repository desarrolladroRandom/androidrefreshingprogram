package com.example.inputcontrols;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

public class HomeWorkActivity extends AppCompatActivity {
    private String selectedDeserts = "Toppings : ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work);
    }

    public void handleCheckbox(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        switch (view.getId()) {
            case R.id.check_chocolate:
                if (checked) selectedDeserts += "Chocolate ";
                break;
            case R.id.check_sprinkles:
                if (checked) selectedDeserts += "Sprinkles ";
                break;
            case R.id.check_crushed_nuts:
                if (checked) selectedDeserts += "Crushed nuts ";
                break;
            case R.id.check_cherries:
                if (checked) selectedDeserts += "Cherries ";
                break;
            case R.id.check_oreo:
                if (checked) selectedDeserts += "Oreo ";
                break;
            default:
                Log.e("HomeWorkActivity", "handleCheckbox error con el id");
                break;
        }
    }

    public void showSelectedDesert(View view) {
        Toast.makeText(this, selectedDeserts, Toast.LENGTH_LONG).show();
    }
}

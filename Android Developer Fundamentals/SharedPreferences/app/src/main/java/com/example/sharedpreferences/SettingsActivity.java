package com.example.sharedpreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {
    private String sharedPrefFile = "com.example.sharedpreferences";
    private SharedPreferences mPreferences;
    private final String COUNT_KEY = "count";
    private final String COLOR_KEY = "color";
    private int mCount = 0;
    private int mColor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        mCount = getIntent().getIntExtra(COUNT_KEY, 0);
        mColor = getIntent().getIntExtra(COLOR_KEY, ContextCompat.getColor(this, R.color.default_background));

    }

    public void reset(View view) {
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.clear();
        preferencesEditor.apply();
    }

    public void update(View view) {
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putInt(COUNT_KEY, mCount);
        preferencesEditor.putInt(COLOR_KEY, mColor);
        preferencesEditor.apply();
    }
}
